export interface FoxData {
    individuals: Record<string, Fox>,
    species: Record<string, FoxSpecies>
    images: Record<string, string []>
  }
  
export interface Fox {
    name: string,
    age: number,
    gender: string,
    species: number 
  }

export interface FoxSpecies {
    name: string,
    habitat: string
  }

  export interface FoxDTO {
    name: string,
    age: number, 
    gender: string,
    species: FoxSpecies,
    images: string []
}