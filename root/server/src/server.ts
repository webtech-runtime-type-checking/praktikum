import express, { Express, Request, Response } from "express";
import cors from "cors"
import { readFileSync }  from "fs"
import { FoxData, FoxDTO } from "./interfaces";
import { parseFoxData } from "./parsing";

const app: Express = express();
app.use(cors())
const port = 3000

const foxData = JSON.parse(readFileSync("fox-data.json", "utf8")) as FoxData

const isCorrect = parseFoxData(foxData)

app.get("/fox/all", (_req: Request, res: Response) => {
  if (!isCorrect) {
    res.status(500)
  }
  res.json(constructFoxes(foxData))
});

app.get("/fox/:foxid", (req: Request, res: Response) => {
  const fox = foxData.individuals[req.params.foxid]
  if (fox === undefined) {
    res.send("no foxes here :(")
  }
  res.json(fox)
});

app.get("/species/:speciesid", (req: Request, res: Response) => {
  const species = foxData.species[req.params.speciesid]
  if (species === undefined) {
    res.send("no species here :(")
  }
  res.json(species)
});

app.get("/fox/:foxid/images", (req: Request, res: Response) => {
  const foxImages = foxData.images[req.params.foxid]

  if (foxImages === undefined) {
    res.send("no fox images here :(")
  }

  res.json(foxImages)
})

const constructFoxes = (foxData: FoxData): FoxDTO [] => {

  const foxes: FoxDTO [] = []
  for (const [foxId, fox] of Object.entries(foxData.individuals)) {
    const imagePaths = foxData.images[foxId]
    const species = foxData.species[fox.species]

    foxes.push({
      ...fox,
      images: imagePaths,
      species: species
    })
  }

  return foxes
}

app.listen(port, () => {
  console.log(`⚡️[server]: Server is running at https://localhost:${port}`);
});

app.use('/', express.static(`${__dirname}/../../client/public/`));
app.use(express.static("img"))