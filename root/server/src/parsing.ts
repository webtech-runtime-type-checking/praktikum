

export const parseFoxData = (foxData: unknown): boolean => {

    return P.record(
        {
            "individuals" : P.recordFixedV(
                P.record({
                    "name" : P.string(),
                    "age" : P.number().lt(14),
                    "gender": P.string(),
                    "species": P.number()
                })
            ),
            "species": P.recordFixedV(
                P.record({
                    "name": P.string(),
                    "habitat": P.string()
                })
            ),
            "images": P.recordFixedV(
                P.array(P.string())
            )
        }
    ).parse(foxData)
}


export const parseFox = (fox: unknown): boolean  => {
    return P.record({
        "name" : P.string(),
        "age" : P.number().lt(14),
        "gender": P.string(),
        "species": P.number()
    }).parse(fox)
}

export const parseFoxImage = (foxImage: unknown): boolean => {
    return P.string().parse(foxImage)
}

export const parseFoxSpecies = (foxSpecies: unknown): boolean => {
    return P.record({
        "name": P.string(),
        "habitat": P.string()
    }).parse(foxSpecies)
}

interface Parser<I> {
    parse: (input: unknown) => input is I 
}

class StringParser implements Parser<string> {
    
    parse: (input: unknown) => input is string = (input): input is string => {
        return typeof input === "string"
    }
    
}

class NumberParser implements Parser<number> {
    private filters: Array<(input: number) => boolean> = []

    constructor() {}

    parse: (input: unknown) => input is number = (input): input is number => {
        const isNumber = typeof input === "number"

        if (isNumber && this.filters !== undefined) {
            return this.filters.reduce(
                (acc: boolean, f: (_: number) => boolean) => acc && f(input), true)
        }
        
        return false 
    }

    lt = (lt: number): Parser<number> => {

        this.filters.push(
            (input) => input < lt
        )
        
        return this
    }

}

class ArrayParser<I> implements Parser<I []> {

    constructor(private elementParser: Parser<I>) {}

    parse = (input: unknown): input is I [] => {
        return Array.isArray(input) && input.reduce((prev, cur) => prev && this.elementParser.parse(cur), true)
    }
}

class RecordParserFixedV<I> implements Parser<Record<string, I>> {

    constructor(private elementParser: Parser<I>) {}

    parse = (input: unknown): input is Record<string, I> => {
        if (!isPlainObject(input)) return false

        const keys = Object.keys(input)
        const vals = Object.values(input)

        return (
            P.array(P.string()).parse(keys) && 
            P.array(this.elementParser).parse(vals)
        )
    }
    
}


class RecordParser implements Parser<Record<string, unknown>> {
    constructor(private mapping: Record<string, Parser<unknown>>) {}

    parse = (input: unknown): input is Record<string, unknown> => {
        if (!isPlainObject(input)) return false 

        let valid = true 

        for (const [key, value] of Object.entries(input)) {
            const parser = this.mapping[key]
            if (parser === undefined) {
                console.log(`key: ${key} has no associated parser`)
                return false
            }
            const parseResult = parser.parse(value)
            if (!parseResult) {
                console.log(`parsing failed for key: ${key}`)
            }
            valid = valid && parseResult
        }

        return valid

    }
}

const isPlainObject = (input: unknown): input is Record<string, unknown> => {
    if (
        typeof input !== "object"   ||
        Array.isArray(input)        ||
        input === null              ||
        input === undefined         
    ) return false 
    return true 
}

class P {

    static string(): StringParser {
        return new StringParser()
    }

    static number(): NumberParser {
        return new NumberParser()
    }

    static record(mapping: Record<string, Parser<unknown>>): RecordParser {
        return new RecordParser(mapping)
    }

    static array<T>(elementParser: Parser<T>) {
        return new ArrayParser(elementParser)
    }

    static recordFixedV<T>(elementParser: Parser<T>) {
        return new RecordParserFixedV(elementParser)
    }
}