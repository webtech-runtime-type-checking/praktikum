import { expect } from "chai"
import { parseFox, parseFoxImage, parseFoxSpecies } from "../src/parsing"

describe("test server-side validation", () => {
    const foxData = {
        "individuals" : {
            "0" : {
                "name" : "Steve",
                "age" : 3,
                "gender" : "male",
                "species" : 0
            },
        },
    
        "species" : {
            "0" : {
                "name" : "Arctic Fox",
                "habitat" : "The Arctic fox has a circumpolar distribution and occurs in Arctic tundra habitats in northern Europe, northern Asia, and North America. "
            },
    
        },
    
        "images" : {
    
            "0": [
                "arctic1.jpg", "arctic2.png"
            ], 
    
            "1": [
                "arctic2.jpg"
            ],
    
        }
    
    }
    
    describe("test server-side individual validation", () => {

        it("validate positive on correct input", () => {
            const foxValid = Object.assign({}, foxData.individuals["0"])
            expect(parseFox(foxValid)).to.be.true
        })

        it("validate negative on wrong object key type", () => {
            const wrongType = Object.assign({}, foxData.individuals["0"]) as Record<string, unknown>
            wrongType.age = "2"
            expect(parseFox(wrongType)).to.be.false
        })

        // side note: furries do not count
        it("validate negative on additional object key ", () => {
            const lawyerFox = Object.assign({}, foxData.species["0"]) as Record<string, unknown>
            lawyerFox["profession"] = "lawyer"
            expect(parseFox(lawyerFox)).to.be.false
        })

        it("validate negative on age > 14 ", () => {
            const eternalFox = Object.assign({}, foxData.individuals["0"])
            eternalFox.age = Number.POSITIVE_INFINITY
            expect(parseFox(eternalFox)).to.be.false
        })

    })


    describe("test server-side species validation", () => {

        it("validate positive on correct input", () => {
            const speciesValid = Object.assign({}, foxData.species["0"])
            expect(parseFoxSpecies(speciesValid)).to.be.true
        })


        it("validate negative on missing interface key", () => {
            const missingKey = Object.assign({}, foxData.individuals["0"]) as Record<string, unknown>
            delete missingKey.habitat
            expect(parseFoxSpecies(missingKey)).to.be.false
        })

    })

    describe("test server-side fox-image validation", () => {

        it("validate positive on correct input", () => {
            const validImage = foxData.images["0"][0]
            expect(parseFoxImage(validImage)).to.be.true
        })

        it ("validate negative on wrong type", () => {
            const nonString = foxData.images["0"].map(_str => 0)[0]
            expect(parseFoxImage(nonString)).to.be.false
        })

    })
})