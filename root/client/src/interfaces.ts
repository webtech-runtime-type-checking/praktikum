export interface FoxData {
    individuals: Record<string, Fox>,
    species: Record<string, FoxSpecies>
    images: Record<string, string []>
  }

export interface FoxBase {
    name: string,
    age: number, 
    gender: string,
}

export type FoxApi = FoxBase & { species: number }
export type Fox = FoxBase & { species: FoxSpecies, images: string [] }
  

export interface FoxSpecies {
    name: string,
    habitat: string
  }