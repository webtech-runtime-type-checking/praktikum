import { z } from "zod"

const foxSchema = z.object({
    "name": z.string(),
    "age": z.number().gt(0),
    "gender": z.string(),
    "species": z.object({
        "name": z.string(), 
        "habitat": z.string()
    }),
    "images": z.array(z.string())
}).strict()

type ZodFox = z.infer<typeof foxSchema>

export const parseFox = (fox: unknown): boolean => {
    try {
        foxSchema.parse(fox)
        return true 
    } catch(e) {
        return false 
    }
}