import { Fox, FoxApi, FoxSpecies } from "./interfaces"

const API_URL = "http://localhost"
const PORT = 3000

document.addEventListener("DOMContentLoaded", async () => {
    const foxes = await getFoxes()
    renderGrid(foxes)
}) 

const getFoxes = async (): Promise<Fox []> => {
    const foxes: Fox [] = await fetchFoxData() 
    return foxes
}

const fetchFoxData = async (): Promise<Fox []> => {
    return (await fetch(`${API_URL}:${PORT}/fox/all`)).json() 
}

const fetchFoxImages = async (foxid: string): Promise<string []> => {
    return (await fetch(`${API_URL}:${PORT}/fox/${foxid}/images`)).json()
}

const fetchFoxSpecies = async (speciesid: number): Promise<FoxSpecies> => {
    return (await fetch(`${API_URL}:${PORT}/species/${speciesid}`)).json()
}

const renderGrid = (foxes: Fox []): void => {
    const container = document.querySelector("#fox-container")
    if (container === null) {
        return 
    }

    const mkGrid = () => {
        const grid = document.createElement("div");
        grid.setAttribute("class", "grid");
        return grid
    }

    const mkFoxCard = (fox: Fox) => {
        const cardElement = document.createElement("div")
        cardElement.innerHTML = renderCard(fox)
        return cardElement
    }

    const foxCards = foxes.map((fox) => mkFoxCard(fox))
    const gridsize = 3 
    
    const grids = [mkGrid()]
    for (const [idx, foxCard] of foxCards.entries()) {
        if (idx > 0 && idx % gridsize === 0) {
            container.appendChild(grids[grids.length - 1])
            grids.push(mkGrid())
        }
        grids[grids.length - 1].appendChild(foxCard)
    }
    container.appendChild(grids[grids.length -1 ])
}

const renderCard = (fox: Fox): string => {
    return `
        <article>
            <header>${fox.name} (${fox.species.name})</header>
            <img src=${fox.images[0]}>
            <footer>
                ${renderFoxAttributes(fox)}
            </footer>
        </article>
    `
}

const renderFoxAttributes = (fox: Fox): string => {
    return `
        <ul>
            <li>Age: ${fox.age}</li>
            <li>Gender: ${fox.gender}</li>
        </ul>
    `
}