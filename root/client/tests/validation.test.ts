import { assert, expect } from "chai"
import { Fox } from "../src/interfaces"
import { parseFox } from "../src/parsing"


describe("test client-side validation with zod", () => {
    
    const foxBase: Record<string, unknown> = {
        "name": "Steve",
        "age": 3,
        "gender": "male",
        "species": {
            "name": "Arctic Fox",
            "habitat": "The Arctic fox has a circumpolar distribution and occurs in Arctic tundra habitats in northern Europe, northern Asia, and North America. "
        },
        "images": ["arctic1.jpg"]
    }

    describe("test client-side fox validation", () => {

        it("validate positive on correct object", () => {
            expect(parseFox(foxBase)).to.be.true 
        })

        it("validate negative on missing key", () => {
            const heWhoShallNotBeNamed = Object.assign({}, foxBase) as Partial<Fox>
            delete heWhoShallNotBeNamed.name
            expect(parseFox(heWhoShallNotBeNamed)).to.be.false
        })

        it("validate negative on additional object key", () => {
            const yakumoRan = Object.assign({}, foxBase) as Record<string, unknown>
            yakumoRan["tail_count"] = 9

            expect(parseFox(yakumoRan)).to.be.false
        })

        it("validate negative on wrong object structure", () => {
            const theyreBothFurryCrittersAndKindaCute = Object.assign({}, foxBase) as Record<string, unknown>
            theyreBothFurryCrittersAndKindaCute.age = {
                age_current_life: 1, 
                life_count: 8
            }
            expect(parseFox(theyreBothFurryCrittersAndKindaCute)).to.be.false
        })

        it("validate negative on wrong key type", () => {
            const wrongKeyType = Object.assign({}, foxBase) as Record<string, unknown>
            wrongKeyType.age = "2"
            expect(parseFox(wrongKeyType)).to.be.false
        })

        it("validate negative on age <= 0", () => {
            const whoCaresAboutLogic = Object.assign({}, foxBase) as Record<string, unknown>
            whoCaresAboutLogic.age = -1
            expect(parseFox(whoCaresAboutLogic)).to.be.false
        })

    })
})