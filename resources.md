# Runtime-Type-Checking
https://github.com/gcanti/io-ts
https://www.typescriptneedstypes.com/

## [Parsing vs. Validation](https://lexi-lambda.github.io/blog/2019/11/05/parse-don-t-validate/)

## [Fixing Typescripts Blindspot](https://www.youtube.com/watch?v=rY_XqfSHock)

- Chapter 1: Demonstration Compile-Time-Check vs. Runtime-Check

- Chapter 2: Implementing Manual Javascript Code to avoid Problem
    - Problem: Bloaty asf 
    - Fix of Issue with Manual Runtime-Type-Checking Code 

- Chapter 3: Zod 
    - Fix of Issue with Zod 
    - Enables Compile Time Checking (Infer)
    - Enables Runtime Checking (safeParse)

- Chapter 4,5: Libraries Yup + Joi