\documentclass[a4paper]{article}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage{parskip}
\usepackage{hyperref}
\usepackage{csquotes}

\hypersetup{
colorlinks=true}


\title{Praktikum Webtech - Runtime Typechecking}

\author{Steffen Lesch, Mario Mollame, Marvin Seck-Eichhorn}

\date{\today}

\begin{document}
\maketitle

\section*{Einleitung \& Vorbereitung}
In diesem Praktikum wollen wir gemeinsam Runtime Typechecking / Data Transfer Objects (DTOs) in der Programmiersprache Typescript anhand einer exemplarischen API für Füchse untersuchen. Dazu muss zunächst eine Reihe vorbereitender Schritte durchgeführt werden um das Projekt zum Laufen zu bringen.

\begin{enumerate}
	\item Klonen sie das \href{https://git.thm.de/webtech-runtime-type-checking/praktikum}{git-repository} in ein Verzeichnis ihrer Wahl.
	
	\item Im geklonten Verzeichnis befindet sich der Ordner \textit{root}, worin auch die beiden Projekte für Client und Server der API liegen. 
	\begin{itemize}
		\item Navigieren sie zunächst in das Verzeichnis \textit{server} und führen den Befehl \textit{npm i} auf der Kommandozeile aus. Der Server kann im Anschluss per \textit{npm run dev} von der Kommandozeile gestartet werden und ist dann standardmäßig unter \href{http://localhost:3000}{http://localhost:3000} zu erreichen. 
		
		\item Als nächstes wird der Schritt für den Client wiederholt. Navigieren sie in das Verzeichnis \textit{client} und führen sie ebenfalls den Befehl \textit{npm i} aus. Mit \textit{npm run build} wird der Client gebaut und ist im Anschluss im \textit{public} Verzeichnis verfügbar.
	\end{itemize}
\end{enumerate}

Damit sollte das Projekt nun laufbereit sein. Beide Projekte sind so konfiguriert, dass bei Speichern von Änderung der Build-Prozess neu angestoßen wird.

Lassen sie das Projekt im Anschluss laufen indem sie den Client bauen und den Server starten. Öffnen sie nun einen Browser ihrer wahl und navigieren sie zu \href{http://localhost:3000}{http://localhost:3000}. Falls alles geklappt hat, sollten sie jetzt in den ersten Genuss süßer Füchse kommen.

\section*{Projektverständnis}

Das Projekt besteht aus einer sehr einfachen \textit{Client-Server-Architektur} mit einer Pseudo-Datenbank im \textit{JSON-Format}. Das Schema für das Projekt ist in Abbildung \ref{fig:arch} zu sehen. Der Server liest nach dem Start die Datenbank aus der JSON-Datei ein und stellt diese dem Client per HTTP-Routen bereit.

\begin{figure}
	\includegraphics[width=\linewidth]{praktikum-architecture}
	\caption{Architektur}
	\label{fig:arch}
\end{figure}

\section*{Aufgaben}

\section{Verständnis Compile- vs. Runtime-Typechecking}
Die erste Aufgabe dient dazu, das Verständnis für die Unterscheidung zwischen Compiletime und Runtime Typechecking zu vertiefen. Navigieren sie hierzu bitte in die Datei \textit{server.ts}. Die Route \textit{/fox/all} dient dazu alle Füchse der Datenbank auf Anfrage auszuliefern. Bearbeiten sie zu dieser Route folgende Aufgaben

\begin{enumerate}
	\item Modifizieren sie die Route so, dass diese einen Compiler-Fehler auslöst und speichern sie im Anschluss ihren Weg als Kommentar.
	
	$ => $ \textbf{\texttt{res.json(foxData.individauls)} - typo}
	
	\item Erzeugen sie als nächstes einen Fehlerfall, bei dem die Route einen Laufzeit-Fehler wirft, sobald der Client die Route aufruft und notieren sie sich auch hier ihren Weg.
	
	$ => $ \textbf{Löschen von \texttt{as FoxData} bei beibehalten von Typo aus 1.}
	
	\item Beschreiben sie nun in eigenen Worten wieso der Compiler nur den ersten Fehlerfall entdecken konnte.
	
	$ => $ \textbf{Da der Compiler die Typinformation durch den Cast verliert, kann dieser nicht mehr erkennen, dass das Objekt eigentlich zum Interface \textit{FoxData} gehört, somit kann dieser auch nicht den Typo erkennen.} 
\end{enumerate}

\section{Effizientere Nutzung der API durch DTOs}
Der Transfer von Informationen zwischen Client und Server wird im Webkontext normalerweise über das HTTP-Protokoll abgewickelt indem der Client dem Server ein \textit{Request} sendet und der Server daraufhin mit einer \textit{Response} antwortet. Dieser Vorgang ist normalerweise sehr kostspielig, da Client und Server meist physisch weit auseinanderliegen und der Transfer dadurch unter einer hohen Round-Trip-Time leidet. Eines der Ziele ist es also, die Anzahl dieser kostspieligen Aufrufe zu minimieren. 

Dazu können DTOs verwendet werden. \textbf{Beschreiben} sie nun zunächst allgemein, wie DTOs dazu beitragen, die Anzahl der Anfragen bezüglich dem Server zu reduzieren.

$ => $ \textbf{Durch Verwenden eines DTO verlagert sich die Konstruktion der vom Client benötigten Daten auf die Seite des Servers. Da der Server direkt mit der Datenquelle verbunden ist, entfallen die HTTP-Requests, welche normalerweise vom Client zum Beschaffen der Einzelteile verwendet werden.}
	
	
Schauen sie sich im Anschluss an, welche Requests vom Client beim Laden der Seite ausgeführt werden und verwenden sie im Anschluss das \textbf{DTO-Pattern} um die Anzahl der Requests auf ein Minimum zu reduzieren. 

Gehen sie dabei wie folgt vor: 

\begin{enumerate}
	\item Definieren sie das DTO indem sie ein Interface im Server-Projekt anlegen.
	
	\item Implementieren sie die Hilfsfunktion \textit{constructFoxes} im Server-Projekt. Diese soll die Rohdaten aus der Datenbank so vorverarbeiten, dass diese direkt vom Client verwendet werden können.
	
	\item Liefern sie mit der \textit{fox/all} Route nun das DTO statt dem ursprünglichen Wert aus.
	
	\item Modifizieren sie im Anschluss den Client Code so, dass dieser mit der modifzierten Response der \textit{fox/all} Route kompatibel wird.
	
	\item Analysieren sie bitte nochmals ihr erstelltes DTO und die gerenderte Webseite. Gibt es eventuell Informationen, die übertragen werden, welche der Client nicht verwendet? Auf was sollte folglich beim Design von DTOs geachtet werden, sodass die verringerte Anzahl an HTTP-Aufrufen sich nicht unnötigerweise negativ auf einen anderen kostspieligen Parameter bei der Übertragung auswirkt? Welcher Parameter ist hiermit gemeint?
	
	$ => $ \textbf{Das Attribut \textit{habitat} wird nirgends vom Client verwendet.} 
	
	$ => $ \textbf{Das DTO sollte nur Daten enthalten, die der Client auch wirklich verwendet. Je größer das verwendete DTO, desto höher ist die mit dem Request verbrauchte Bandbreite, was sich ebenfalls negativ auf die Übertragungsgeschwindigkeit auswirken kann}
\end{enumerate}

\section{Verbessern der Typsicherheit der API}

Wie bereits aus der vorherigen Aufgabe hervorgegangen ist, ist die API nicht besonders robust, was Laufzeitsicherheit anbelangt. In diesem Aufgabenteil soll dem entgegengewirkt werden

\subsection{Identifizieren der API Schwachstellen}
Auf der Server-Seite ist das Einlesen der \textbf{JSON-Datei} eine mögliche Fehlerquelle, während es auf der Client-Seite Probleme beim \textbf{Verarbeiten der Request-Antworten} geben kann. \textbf{Erklären sie nun in eigenen Worten}, warum die beiden fett markierten Punkte Quellen für Laufzeitfehler sind und liefern sie im Anschluss eine verallgemeinerte Beschreibung dieser Art von Fehlerquellen.

$ => $ \textbf{Bei beiden Stellen im Programm handelt es sich um eine Schnittstelle zu einem anderen System, dessen Übertragung es nicht erlaubt, mit Typescript kompatible Typdefinitionen zu Übertragen}

\subsection{Typescript / Javascript Bordmittel}
Wir werden zunächst versuchen, die Schwachstellen exemplarisch mittels Bordmitteln von Javascript zu beheben. Navigieren sie in das Server-Projekt und öffnen sie die Datei \textit{validation.ts}. Hierin finden sie noch nicht implementierte Funktionen zur Validierung der Objektstruktur der JSON-Datenbank. 

\begin{enumerate}
	\item Implementieren sie diese Funktionen, sodass diese zur Validierung von FoxData Objekten verwendet werden können.
	
	\item Führen sie die dazu gehörigen Tests aus, indem sie den Befehl \textit{npm run tests} ausführen und überprüfen sie, ob alle Tests für die Funktion bestanden werden.
	
	\item Bauen sie die Funktion in die Route \textit{/fox/all} ein um diese robuster gegenüber \enquote{kaputten} Datenbankeinträgen zu machen.
	
	\item Ihre Funktion besteht alle Tests. Gibt es Lücken bei der Testabdeckung? Beschreiben sie, was alles getestet werden müsste, damit die Tests einen zu 100\% funktionierenden Validator widerspiegeln. 
	
	$ => $ \textbf{Hängt von der Implementierung ab. Grundsätzlich muss aber jedes Attribut auf Typ und vorhandensein geprüft werden. Überflüssige Attribute sollten ebenfalls eliminiert werden.}
	
	\item Überlegen sie, ob ihnen zu ihrem geschriebenen Validator noch Wege einfallen diesen auszutricksen.
	
	$ => $ \textbf{Zur Musterlösung: Dieser prüft z.b nicht ob überflüssige Attribute vorhanden sind.}
		
\end{enumerate}

\subsection{Externe Libraries}

Wir gehen nun einen Schritt weiter und lassen die Hauptarbeit des Runtime Typechecking von externen Libraries erledigen. Ziel dieser Aufgabe ist das Parsing der Request Antworten des Servers, bevor diese vom Client weiterverarbeitet werden um die Seite mit Inhalt zu füllen. Navigieren sie dazu bitte zunächst in das Client-Projekt.

Zunächst identifizieren wir dazu die Stellen, an denen die Requests ausgeführt werden. Wir finden dabei folgende Route:

\begin{itemize}
	\item \texttt{fetchFoxData = async (): Promise<Fox []>}
\end{itemize}

Schauen sie in den Code, werden sie feststellen, dass diese Route in Wahrheit ein \verb|Promise<any>| zurückgibt. Aufrufende Funktionen laufen hier in das Risiko einen nicht erwarteten Typ als return Wert der Funktion zu empfangen und bei Verwendung dann Laufzeitfehler auszulösen. Zum Vorbeugen dieses Fehlers verwenden wir nun die Library \href{https://github.com/colinhacks/zod}{zod}.

Bearbeiten sie nun bitte folgende Aufgaben:

\begin{enumerate}
	\item Navigieren sie in \textit{parsing.ts} und implementieren sie die dort definierten Funktionen zum Parsing der HTTP-Antworten mit Zod.
	
	\item Fügen sie die Funktion in den Client Code ein.
	
	\item Führen sie auch hier wieder die dazugehörigen Tests aus und prüfen sie, ob ihre Funktion gültig ist.
	
	\item Ein Highlight der Library ist das Ableiten eines statischen Typescript Typen aus einem definierten Validator. Beschreiben sie den Effekt, der dieses Feature auf die Implementierung von Runtime Typechecking in Typescript Projekten hat.
	
	$ => $ \textbf{Da sich der Typ aus dem Validator ableiten lässt, muss kein vom Validator entkoppelter Typ / Interface erstellt werden. Der generierte Typ hat zwei wichtige implizite Vorteile zu einem normalen Typ:
		\begin{itemize}
			\item Automatische Synchronisation zwischen Validator und Typ, da der Validator den Typ bestimmt und nicht umgekehrt.
			\item Ein abgeleiteter Typ garantiert, dass dieser zur Laufzeit validierbar ist.
		\end{itemize}
	}
	
\end{enumerate}

\section{Vergleich der Ansätze}

Vergleichen sie den manuellen Ansatz mit dem Library Ansatz. Gehen sie dabei jeweils auf die Punkte \textbf{Flexibilität} und \textbf{Aufwand} ein.

$ => $ 

\begin{enumerate}
	\item \textbf{\textit{Flexibilität}: Beide Ansätze verwenden ein ähnliches Prinzip, welches darauf aufbaut aus simplen Parsern komplexe zusammenzubauen. Dadurch entsteht eine relativ hohe flexibilität beider Ansätze gegenüber dem Einsatzgebiet. Zod ist natürlich aber viel ausgereifter und bietet eine deutlich höhere Flexibilität gegenüber verschiedener Validierungsszenarien.}
	
	\item \textbf{\textit{Aufwand}: Wird der initiale Implementierungsaufwand ausgelassen, so lassen sich mit beiden Ansätzen mit ähnlichem Aufwand Schemata zur Validierung definieren. Entscheidender Vorteil von Zod ist allerdings hier die Möglichkeit einen statischen Typen aus dem Validator abzuleiten.}
	
\end{enumerate}

\section{Optional: Validierung vs. Parsing}

Lesen sie sich den Artikel \href{https://lexi-lambda.github.io/blog/2019/11/05/parse-don-t-validate/}{https://lexi-lambda.github.io/blog/2019/11/05/parse-don-t-validate/} durch und beantworten sie folgende Fragen. 

\begin{enumerate}
	\item Worin liegt laut dem Artikel der Unterschied zwischen Validierung und Parsing? 
	\item Welchen Ansatz haben sie in ihrer Implementierung verwendet und warum? 
	\item Würden sie dem Slogan des Authors \textit{Parse don't validate} zustimmen?
\end{enumerate}	

\end{document}
